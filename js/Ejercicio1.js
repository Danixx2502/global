function calcularPulsaciones() {
    var edad = parseInt(document.getElementById("edad").value);
    var resultado = "Pulsaciones promedio: ";
    var totalPulsaciones = "Total de pulsaciones en la vida: ";

    if (edad >= 0 && edad <= 1) {
        resultado += "entre 140 y 160 por minuto.";
        totalPulsaciones += (edad * 365.25 * (150)) + " pulsaciones.";
    } else if (edad >= 2 && edad <= 12) {
        resultado += "entre 110 y 115 por minuto.";
        totalPulsaciones += (edad * 365.25 * (112.5)) + " pulsaciones.";
    } else if (edad >= 13 && edad <= 40) {
        resultado += "entre 70 y 80 por minuto.";
        totalPulsaciones += (edad * 365.25 * (75)) + " pulsaciones.";
    } else {
        resultado += "entre 60 y 70 por minuto.";
        totalPulsaciones += (edad * 365.25 * (65)) + " pulsaciones.";
    }

    document.getElementById("resultado").textContent = resultado;
    document.getElementById("totalPulsaciones").textContent = totalPulsaciones;
}