function generarCalificaciones() {
    var calificaciones = [];
    var totalAlumnos = 35;
  
    for (var i = 0; i < totalAlumnos; i++) {
      calificaciones.push(Math.random() * 10);
    }
  
    mostrarCalificaciones(calificaciones);
    calcularEstadisticas(calificaciones);
  }
  
  function mostrarCalificaciones(calificaciones) {
    var calificacionesDiv = document.getElementById('calificaciones');
    calificacionesDiv.innerHTML = 'Calificaciones de los Alumnos: ' + calificaciones.join(', ');
  }
  
  function calcularEstadisticas(calificaciones) {
    var totalAlumnos = calificaciones.length;
    var noAprobados = calificaciones.filter(calificacion => calificacion < 7).length;
    var aprobados = totalAlumnos - noAprobados;
    var promedioNoAprobados = calificaciones.reduce((acc, val) => val < 7 ? acc + val : acc, 0) / noAprobados || 0;
    var promedioAprobados = calificaciones.reduce((acc, val) => val >= 7 ? acc + val : acc, 0) / aprobados || 0;
    var promedioGeneral = calificaciones.reduce((acc, val) => acc + val, 0) / totalAlumnos || 0;
  
    document.getElementById('totalAlumnos').textContent = totalAlumnos;
    document.getElementById('noAprobados').textContent = noAprobados;
    document.getElementById('aprobados').textContent = aprobados;
    document.getElementById('promedioNoAprobados').textContent = promedioNoAprobados.toFixed(2);
    document.getElementById('promedioAprobados').textContent = promedioAprobados.toFixed(2);
    document.getElementById('promedioGeneral').textContent = promedioGeneral.toFixed(2);
  }